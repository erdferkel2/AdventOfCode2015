#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PW_LEN  (8)

#define FALSE   (0)
#define TRUE    (1)

typedef char bool;

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day11(0);
        }
        else{
            return day11(1);
        }
    }
    else{
        return day11(0);
    }
}

int passwordValid(char* pw){
    int ii, jj;
    int kk = 0;
    bool threeRising = FALSE;
    bool twoPairs = FALSE;

    char pairs[PW_LEN];

    for(ii=0;ii<PW_LEN;ii++){
        // forbidden letters
        if(pw[ii] == 'i'  || pw[ii] == 'o' || pw[ii] == 'l'){
            return FALSE;
        }

        // increasing straight
        if(ii>1){
            // I'm sure this does create a lot of false positives
            // It's still here, because it canceled out with the other
            // conditions, and I did not bother to fix it
            if(pw[ii-2] == pw[ii-1]-1 && pw[ii-1] == pw[ii]-1)
                threeRising = TRUE;
                //printf("rising: %c%c%c\n",pw[ii-2],pw[ii-1],pw[ii]);
        }

        if(ii>0){
            if(pw[ii] == pw[ii-1]){
                pairs[kk] = pw[ii];
                kk++;
                //printf("pair: %c%c\n",pw[ii-1],pw[ii]);
            }
        }
    }

    // dirty dirty hacks
    // I`m dumb and can't think of a better way
    for(ii=0;ii<kk;ii++){
        for(jj=0;jj<kk;jj++){
            if(pairs[ii] != pairs[jj])
                twoPairs = TRUE;
        }
    }

    if(twoPairs && threeRising)
        return TRUE;

    return FALSE;
}

void incPw(char* pw){
    int ii;;
    for(ii=PW_LEN-1;ii>-1;ii--){
        pw[ii]++;
        if(pw[ii] > 'z'){
            pw[ii] = 'a';
        }
        else{
            return;
        }
    }
}

int day11(int part){
    char pw[] = "hepxcrrq";

    int ii;

    incPw(pw);

    while(!passwordValid(pw)){
        incPw(pw);
    }

    printf("%s\n",pw);

    incPw(pw);

    while(!passwordValid(pw)){
        incPw(pw);
    }

    printf("%s\n",pw);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>

#define TRUE  (1)
#define FALSE (0)

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day5_A();
        }
        else{
            return day5_B();
        }
    }
    else{
        return day5_A();
    }
}

int day5_A()
{
    FILE * input;
    char cc = ' ';
    char last = ' ';
    int ii = 0;
    int len = 16;
    int nice = 0;
    int vowels = 0;
    char string[17];

    string[16] = '\0';

    char noBads = TRUE;
    char doubleLetter = FALSE;

    input = fopen("input.txt", "r");

    cc = getc(input);

    while(cc != EOF){
        // read string
        for(ii=0;ii<len;ii++){
            string[ii] = cc;
            cc = getc(input);
        }

        cc = getc(input); // get rid of pesky newline

        printf("Read: %s\n",string);

        // check for bad substrings ab, cd, pq, or xy,
        last = string[0];
        for(ii = 1;ii<len;ii++){
            if(last == 'a' && string[ii] == 'b'){
                noBads = FALSE;
            }
            if(last == 'c' && string[ii] == 'd'){
                noBads = FALSE;
            }
            if(last == 'p' && string[ii] == 'q'){
                noBads = FALSE;
            }
            if(last == 'x' && string[ii] == 'y'){
                noBads = FALSE;
            }
            last = string[ii];
        }

        if(noBads){
            // check for doubles
            last = string[0];
            for(ii=1;ii<len;ii++){
                if(last == string[ii]){
                    doubleLetter = TRUE;
                }
                last = string[ii];
            }

            if(doubleLetter){
                // check for vowels
                vowels = 0;
                for(ii=0;ii<len;ii++){
                    if(string[ii] == 'a' || string[ii] == 'e' || string[ii] == 'i' || string[ii] == 'o' || string[ii] == 'u'){
                        vowels++;
                    }
                }
                if(vowels >= 3){
                    printf("nice++\n");
                    nice++;
                }
            }
        }

        noBads = TRUE;
        doubleLetter = FALSE;
        //cc = getc(input);
    }

    printf("nice Strings: %d\n",nice);

    return 0;
}

int day5_B()
{
    FILE * input;
    char cc = ' ';
    char last = ' ';
    int ii = 0;
    int jj = 0;
    int len = 16;
    int nice = 0;
    int vowels = 0;
    char string[17];

    string[16] = '\0';

    char strangeDouble = FALSE;

    input = fopen("input.txt", "r");

    cc = getc(input);

    while(cc != EOF){
        // read string
        for(ii=0;ii<len;ii++){
            string[ii] = cc;
            cc = getc(input);
        }

        cc = getc(input); // get rid of pesky newline

        // check doubles with char inbetween
        last = string[0];
        for(ii=2;ii<len;ii++){
            if(last == string[ii]){
                strangeDouble = TRUE;
            }
            last = string[ii-1];
        }

        if(strangeDouble){
                char pair[2];
            for(ii=0;ii<15;ii++){
                pair[0] = string[ii];
                pair[1] = string[ii+1];
                for(jj=0;jj<15;jj++){
                    if(jj != ii && jj+1 != ii && jj-1 != ii){
                        if(string[jj] == pair[0] && string[jj+1] == pair[1]){
                            nice++;
                            printf("%s is nice\n",string);
                            goto end;
                        }
                    }
                }
            }
        }

        end:

        strangeDouble = FALSE;
    }

    printf("nice Strings: %d\n",nice);

    return 0;
}

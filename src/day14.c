#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RUN_COUNT (2503)
#define DEER_COUNT (9)

struct deerType{
    char name[10];
    int speed;
    int timeOn;
    int timeOff;
    int remainingOn;
    int remainingOff;
    int state;  // 0 = running, 1 = waiting
    int distance;
    int points;
};

struct deerType deers[DEER_COUNT];

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day14(0);
        }
        else{
            return day14(1);
        }
    }
    else{
        return day14(0);
    }
}

void readFile(){
    FILE* input;

    input = fopen("input.txt","r");

    char string[100];

    char* name;
    char* speedS;
    char* timeOnS;
    char* timeOffS;
    int speed, timeOn, timeOff;

    char* delim = " ";

    int ii = 0;

    while(fgets(string,sizeof(string),input)){
        // get rid of \n
        string[strlen(string)-1] = 0;
        // scan string
        name = strtok(string, delim);
        strtok(NULL, delim); // skip " can "
        strtok(NULL, delim); // skip " fly "
        speedS = strtok(NULL, delim);
        strtok(NULL, delim); // skip " km/s "
        strtok(NULL, delim); // skip " for "
        timeOnS = strtok(NULL, delim);
        strtok(NULL, delim); // skip " seconds, "
        strtok(NULL, delim); // skip " but "
        strtok(NULL, delim); // skip " then "
        strtok(NULL, delim); // skip " must "
        strtok(NULL, delim); // skip " rest "
        strtok(NULL, delim); // skip " for "
        timeOffS = strtok(NULL, delim);

        speed = atoi(speedS);
        timeOn = atoi(timeOnS);
        timeOff = atoi(timeOffS);

        strcpy(deers[ii].name,name);
        deers[ii].speed = speed;
        deers[ii].timeOn = timeOn;
        deers[ii].timeOff = timeOff;
        deers[ii].remainingOn  = timeOn;
        deers[ii].remainingOff = 0;
        deers[ii].state = 0;
        deers[ii].distance = 0;

        printf("%s/%d/%d/%d/\n",deers[ii].name,deers[ii].speed,deers[ii].timeOn,deers[ii].timeOff);

        ii++;
    }
}

int getLeader(){
    int ii;
    int best = 0;

    for(ii=0;ii<DEER_COUNT;ii++){
        if(deers[ii].distance > best){
            best = deers[ii].distance;
        }
    }

    return best;
}

int day14(int part){
    int ii,jj;
    int max;

    readFile();

    for(ii=0;ii<RUN_COUNT;ii++){
        for(jj=0;jj<DEER_COUNT;jj++){
            if(deers[jj].state == 0){
                deers[jj].remainingOn--;
                deers[jj].distance+=deers[jj].speed;
            }
            else{
                deers[jj].remainingOff--;
            }
            //if(jj==8) printf("%s on:%d off:%d\n",deers[jj].name,deers[jj].remainingOn,deers[jj].remainingOff);
            if(deers[jj].remainingOn == 0){
                deers[jj].remainingOn = deers[jj].timeOn;
                deers[jj].state = 1;
            }
            if(deers[jj].remainingOff == 0){
                deers[jj].remainingOff = deers[jj].timeOff;
                deers[jj].state = 0;
            }
        }

       max = getLeader();

       for(jj=0;jj<DEER_COUNT;jj++){
            if(deers[jj].distance == max){
                deers[jj].points++;
            }
        }
    }



    for(ii=0;ii<DEER_COUNT;ii++){
        printf("%s @ %d\n",deers[ii].name,deers[ii].points);
    }

    return 0;
}

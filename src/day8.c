#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day8_A();
        }
        else{
            return day8_B();
        }
    }
    else{
        return day8_A();
    }
}

int day8_A(void){
    FILE * input;
    char string[100];

    int ii,jj;
    int pos;
    int codeLen = 0;
    int charLen = 0;

    input = fopen("input.txt", "r");

    for(jj=0;jj<300;jj++){
        pos = 0;
        while(string[pos] != '\n'){
            pos++;
            string[pos] = getc(input);
            printf("%c",string[pos]);
        }
        printf("\n");
        codeLen++;
        for(ii=2;ii<pos;ii++){
            if(string[ii] == '\\'){
                if(string[ii+1] == '\\' || string[ii+1] == '"'){
                    ii++;
                    codeLen++;
                }
                else if(string[ii+1] == 'x'){
                    ii += 3;
                    codeLen += 3;
                }
                else {
                    printf("stray \\\n");
                }
            }
            else if(string[ii] == '"'){
                codeLen++;
                break;
            }
            codeLen++;
            charLen++;
        }
        printf("%d/%d\n\n",codeLen,charLen);
    }

    return 0;
}

int day8_B(void){
    FILE * input;
    char string[100];

    int ii,jj;
    int pos;
    int codeLen = 0;
    int charLen = 0;

    input = fopen("input.txt", "r");

    for(jj=0;jj<300;jj++){
        pos = 0;
        while(string[pos] != '\n'){
            pos++;
            string[pos] = getc(input);
            printf("%c",string[pos]);
        }
        printf("\n");
        codeLen++;
        charLen+=3;
        for(ii=2;ii<pos;ii++){
            if(string[ii] == '\\'){
                if(string[ii+1] == '\\' || string[ii+1] == '"'){
                    ii++;
                    codeLen++;
                    charLen+=3;
                }
                else if(string[ii+1] == 'x'){
                    ii += 3;
                    codeLen += 3;
                    charLen += 4;
                }
                else {
                    printf("stray \\\n");
                }
            }
            else if(string[ii] == '"'){
                charLen+=3;
                codeLen++;
                break;
            }
            codeLen++;
            charLen++;
        }
        printf("%d/%d\n\n",codeLen,charLen);
    }

    return 0;
}

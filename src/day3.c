#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct NodeType{
    struct node *next;
    int x;
    int y;
} node;

node Head;

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day3(0);
        }
        else{
            return day3(1);
        }
    }
    else{
        return day3(0);
    }
}

printNode(node* N){
    printf("%d/%d\n",N->x,N->y);
}

printAll(node* N){
    printNode(N);
    if(N->next)
        printAll(N->next);
}

void insertNode(int x, int y){
    node * c = &Head;
    if((c->x == x) && (c->y == y)) return;
    while(c->next){
        c = c->next;
        if((c->x == x) && (c->y == y)) return;
    }

    node* newN = (node*) malloc(sizeof(struct NodeType));
    newN->x=x;
    newN->y=y;
    newN->next = NULL;
    c->next = newN;
}

int countNodes(){
    int count = 1;
    node * c = &Head;
    while(c->next){
        count++;
        c = c->next;
    }
    return count;
}


int day3(int part){
    FILE * input;
    int x, y;
    int xr, yr;
    char cc;
    char robo;

    x = 0; y = 0;
    xr = 0; yr = 0;
    cc = 1;
    robo = 0;
    input = fopen("input.txt","r");

    Head.next = NULL;

    Head.x = x;
    Head.y = y;

    while(cc != EOF){
        cc = getc(input);

        if(robo == 1){
            switch(cc){
                case '^': xr++; break;
                case 'v': xr--; break;
                case '>': yr++; break;
                case '<': yr--; break;
            }
            robo = 0;
            insertNode(xr,yr);
        }
        else{
            switch(cc){
                case '^': x++; break;
                case 'v': x--; break;
                case '>': y++; break;
                case '<': y--; break;
            }
            robo = 1;
            insertNode(x,y);
        }


    }

    printAll(&Head);
    printf("count: %d\n", countNodes());

    return 0;
}

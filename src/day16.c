#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define A_COUNT 500

struct profile{
    int children;
    int cats;
    int samoyeds;
    int pomeranians;
    int aktias;
    int vizslas;
    int goldfish;
    int trees;
    int cars;
    int perfumes;
    int valid;
};

struct profile aunts[A_COUNT];

struct profile search;

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day16(0);
        }
        else{
            return day16(1);
        }
    }
    else{
        return day16(0);
    }
}

void parseFile(){
    FILE* input;
    input = fopen("input.txt","r");

    char string[100];
    int ii;
    int line = 0;

    char* nameA;
    char* nameB;
    char* nameC;

    int numberA;
    int numberB;
    int numberC;

    char* delim = " ";

    while(fgets(string,sizeof(string),input)){
        line++;
        // strip , and :
        string[strlen(string)-1] = 0;
        for(ii=0;ii<strlen(string);ii++){
            if(string[ii] == ',' || string[ii] == ':'){
                string[ii] = ' ';
            }
        }
        // format: Sue xxx [name] [number] [name] [number] [name] [number]

        strtok(string, delim); // skip Sue
        strtok(NULL, delim); // skip number
        nameA = strtok(NULL, delim);
        numberA = atoi(strtok(NULL,delim));
        nameB = strtok(NULL, delim);
        numberB = atoi(strtok(NULL,delim));
        nameC = strtok(NULL, delim);
        numberC = atoi(strtok(NULL,delim));

        //printf("%s/%d/%s/%d/%s/%d/\n",nameA, numberA, nameB, numberB, nameC, numberC);

        fillAttribute(line, nameA, numberA);
        fillAttribute(line, nameB, numberB);
        fillAttribute(line, nameC, numberC);

        //printAunt(line);
    }
}

void fillAttribute(int line, char* name, int number){
    if(name[0] == 'a'){
        // akita
        aunts[line].aktias = number;
    }
    else if(name[0] == 'c'){
        if(name[1] == 'a'){
            if(name[2] == 'r'){
                // car
                aunts[line].cars = number;
            }
            else if(name[2] == 't'){
                // cat
                aunts[line].cats = number;
            }
        }
        else if(name[1] == 'h'){
            // children
            aunts[line].children = number;
        }
    }
    else if(name[0] == 'g'){
        // goldfish
        aunts[line].goldfish = number;
    }
    else if(name[0] == 'p'){
        if(name[1] == 'e'){
            //perfumes
            aunts[line].perfumes = number;
        }
        else if(name[1] == 'o'){
            // pomeranians
            aunts[line].pomeranians = number;
        }
    }
    else if(name[0] == 's'){
        // samoyeds
        aunts[line].samoyeds = number;
    }
    else if(name[0] == 't'){
        // trees
        aunts[line].trees = number;
    }
    else if(name[0] == 'v'){
        // vizslas
        aunts[line].vizslas = number;
    }
}

void initArrays(){
    int ii;

    for(ii=0;ii<A_COUNT;ii++){
        aunts[ii].aktias = -1;
        aunts[ii].cars = -1;
        aunts[ii].cats = -1;
        aunts[ii].children = -1;
        aunts[ii].goldfish = -1;
        aunts[ii].perfumes = -1;
        aunts[ii].pomeranians = -1;
        aunts[ii].samoyeds = -1;
        aunts[ii].trees = -1;
        aunts[ii].vizslas = -1;
        aunts[ii].valid = 1;
    }

    search.aktias = 0;
    search.cars = 2;
    search.cats = 7;
    search.children = 3;
    search.goldfish = 5;
    search.perfumes = 1;
    search.pomeranians = 3;
    search.samoyeds = 2;
    search.trees = 3;
    search.vizslas = 0;
}

void printAunt(int pos){
    printf("%d: aktias:%d cars:%d cats:%d children:%d goldfish:%d perfumes:%d pomeranians:%d samoyeds:%d trees:%d vizslas:%d\n",pos ,aunts[pos].aktias,aunts[pos].cars, aunts[pos].cats, aunts[pos].children, aunts[pos].goldfish, aunts[pos].perfumes, aunts[pos].pomeranians, aunts[pos].samoyeds, aunts[pos].trees, aunts[pos].vizslas);
}

int day16(int part){
    int ii,jj;

    initArrays();
    parseFile();

    for(ii=0;ii<A_COUNT;ii++){
        if(aunts[ii].aktias >= 0 && aunts[ii].aktias != search.aktias){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].cars >= 0 && aunts[ii].cars != search.cars){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].cats >= 0 && aunts[ii].cats <= search.cats){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].children >= 0 && aunts[ii].children != search.children){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].goldfish >= 0 && aunts[ii].goldfish >= search.goldfish){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].perfumes >= 0 && aunts[ii].perfumes != search.perfumes){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].pomeranians >= 0 && aunts[ii].pomeranians >= search.pomeranians){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].samoyeds >= 0 && aunts[ii].samoyeds != search.samoyeds){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].trees >= 0 && aunts[ii].trees <= search.trees){
            aunts[ii].valid = 0;
        }
        if(aunts[ii].vizslas >= 0 && aunts[ii].vizslas != search.vizslas){
            aunts[ii].valid = 0;
        }
    }

    for(ii=0;ii<A_COUNT;ii++){
        if(aunts[ii].valid){
            //printf("%d\n",ii);
            printAunt(ii);
        }
    }


    return 0;
}

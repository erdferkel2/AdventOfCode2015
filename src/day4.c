#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day4(0);
        }
        else{
            return day4(1);
        }
    }
    else{
        return day4(0);
    }
}

int day4(int part){
    int ii;
    int counter;
    unsigned char digest[16];
    char* secret;
    char* string[33];
    char md5string[33];
    MD5_CTX context;
    int zeroes;

    zeroes = 0;
    counter = 0;

    secret = "iwrupvqb";

    while(zeroes < 6){
        zeroes = 0;

        counter++;

        if(counter == 407673) counter++;

        strcpy(string, "");
        sprintf(string, "%s%d",secret, counter);

        if(counter%1000 == 0) printf("%d\n",counter);


        MD5_Init(&context);
        MD5_Update(&context, string, strlen(string));
        MD5_Final(digest, &context);



        for(ii = 0; ii < 16; ++ii)
            sprintf(&md5string[ii*2], "%02x", (unsigned int)digest[ii]);

        for(ii = 0; ii < 6; ii++){
            if(md5string[ii] != '0'){
                break;
            }
            else{
                zeroes++;
            }
        }
    }

    printf("%s\n%s\n%d\n",string, md5string,counter);

    return 0;
}

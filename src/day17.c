#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define NODE_COUNT 20

int count = 0;
int minDepth = INT_MAX;
int countDepth = 0;

int bucket[NODE_COUNT];

struct set{
    int nodes[NODE_COUNT];
};

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day17(0);
        }
        else{
            return day17(1);
        }
    }
    else{
        return day17(0);
    }
}

void initSet(struct set* s){
    int ii;
    for(ii=0;ii<NODE_COUNT;ii++){
        s->nodes[ii] = 1;
    }
}

void readFile_day17(){
    FILE* fp;
    int ii;

    fp = fopen("input.txt","r");
    for(ii=0;ii<NODE_COUNT;ii++){
        fscanf(fp,"%d",&bucket[ii]);
        getc(fp);
    }

    for(ii=0;ii<NODE_COUNT;ii++){
        printf("%d\n",bucket[ii]);
    }
}

void printPath(struct set* s){
    int ii;

    for(ii=0;ii<NODE_COUNT;ii++){
        printf("%d",bucket[s->nodes[ii]]);
    }

    printf("\n");
}

void createPath(int remainder, struct set s, int depth){
    int ii;

    if(remainder < 1){
        if(remainder == 0){
            count++;
            if(depth < minDepth){
                minDepth = depth;
                countDepth = 0;
            }
            if(depth == 4){
                countDepth++;
            }
        }
        return;
    }

    for (ii=0;ii<NODE_COUNT;ii++) {
        if (s.nodes[ii] == 1){
            s.nodes[ii] = 0;
            createPath(remainder - bucket[ii], s, depth+1);
        }
    }
}


int day17(int part){
    struct set s;

    readFile_day17();

    initSet(&s);

    createPath(150, s, 0);

    printf("combinations: %d\nMin Depth: %d\nNumber: %d\n",count,minDepth,countDepth);

    return 0;
}

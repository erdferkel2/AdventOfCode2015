#include <stdio.h>
#include <stdlib.h>

#define SIZE 100
#define STEPS 100

char lights[SIZE][SIZE];
char lightsNEW[SIZE][SIZE];

int  day18(int part);
void readFile(void);
void printLights(void);
void step(int part);
int  getNeighbors(int col, int row);
int getNeighborsShort(int col, int row);
int  countLightsOn(void);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day18(part);
    }
    else{
        return day18(0);
    }
}

void readFile(void){
    FILE* fp;
    char cc;
    int cnt = 0;

    fp = fopen("input/day18.txt","r");

    while(cc != EOF){
        cc = getc(fp);

        if(cc == '#'){
            lights[cnt%SIZE][cnt/SIZE] = cc;
        }
        else if(cc == '.'){
            lights[cnt%SIZE][cnt/SIZE] = cc;
        }
        else if(cc == '\n'){
            continue;
        }
        else{
            printf("ERROR in input File!\n");
            printf("char: %c @cnt %d\n",cc,cnt);
            break;
        }

        cnt++;
        if(cnt >= SIZE*SIZE){
            break;
        }
    }
}

void printLights(void){
    for(int row = 0; row < SIZE; row++){
        for(int col = 0; col < SIZE; col++){
            printf("%c",lights[col][row]);
        }
        printf("\n");
    }
}

void step(int part){
    for(int row = 0; row < SIZE; row++){
        for(int col = 0; col < SIZE; col++){
            int neighbors = getNeighborsShort(col, row);
            if(lights[col][row] == '.'){
                if(neighbors == 3){
                    lightsNEW[col][row] = '#';
                }
                else{
                    lightsNEW[col][row] = '.';
                }
            }
            else if (lights[col][row] == '#'){
                if(neighbors != 3 && neighbors != 2){
                    lightsNEW[col][row] = '.';
                }
                else{
                    lightsNEW[col][row] = '#';
                }
            }
        }
    }

    for(int row = 0; row < SIZE; row++){
        for(int col = 0; col < SIZE; col++){
            lights[col][row] = lightsNEW[col][row];
        }
    }

    if(part != 0){ // four corners always on
        lights[0][0] = '#';
        lights[0][SIZE-1] = '#';
        lights[SIZE-1][0] = '#';
        lights[SIZE-1][SIZE-1] = '#';
    }
}

int getNeighbors(int col, int row){
    int cnt = 0;

    // handle points not on edges
    if(col > 0 && col < SIZE-1 && row > 0 && row < SIZE-1){
        if(lights[col-1][row-1] == '#') cnt++; // top left
        if(lights[col]  [row-1] == '#') cnt++; // top center
        if(lights[col+1][row-1] == '#') cnt++; // top right

        if(lights[col-1][row] == '#')   cnt++; // center left
        if(lights[col+1][row] == '#')   cnt++; // center right

        if(lights[col-1][row+1] == '#') cnt++; // bot left
        if(lights[col]  [row+1] == '#') cnt++; // bot center
        if(lights[col+1][row+1] == '#') cnt++; // bot right
    }
    // handle corners
    else if(col == 0 && row == 0){ //top left corner
        if(lights[col+1][row] == '#')   cnt++; // center right

        if(lights[col]  [row+1] == '#') cnt++; // bot center
        if(lights[col+1][row+1] == '#') cnt++; // bot right
    }
    else if(col == 0 && row == SIZE-1){ // bot left corner
        if(lights[col]  [row-1] == '#') cnt++; // top center
        if(lights[col+1][row-1] == '#') cnt++; // top right

        if(lights[col+1][row] == '#')   cnt++; // center right
    }
    else if(col == SIZE-1 && row == 0){ //top right corner
        if(lights[col-1][row] == '#')   cnt++; // center left

        if(lights[col-1][row+1] == '#') cnt++; // bot left
        if(lights[col]  [row+1] == '#') cnt++; // bot center
    }
    else if(col == SIZE-1 && row == SIZE-1){ // bot right corner
        if(lights[col-1][row-1] == '#') cnt++; // top left
        if(lights[col]  [row-1] == '#') cnt++; // top center

        if(lights[col-1][row] == '#')   cnt++; // center left
    }
    // handle edges
    else if(col == 0){ // left edge
        if(lights[col]  [row-1] == '#') cnt++; // top center
        if(lights[col+1][row-1] == '#') cnt++; // top right

        if(lights[col+1][row] == '#')   cnt++; // center right

        if(lights[col]  [row+1] == '#') cnt++; // bot center
        if(lights[col+1][row+1] == '#') cnt++; // bot right
    }
    else if(col == SIZE-1){ // right edge
        if(lights[col-1][row-1] == '#') cnt++; // top left
        if(lights[col]  [row-1] == '#') cnt++; // top center

        if(lights[col-1][row] == '#')   cnt++; // center left

        if(lights[col-1][row+1] == '#') cnt++; // bot left
        if(lights[col]  [row+1] == '#') cnt++; // bot center
    }
    else if(row == 0){ // top edge
        if(lights[col-1][row] == '#')   cnt++; // center left
        if(lights[col+1][row] == '#')   cnt++; // center right

        if(lights[col-1][row+1] == '#') cnt++; // bot left
        if(lights[col]  [row+1] == '#') cnt++; // bot center
        if(lights[col+1][row+1] == '#') cnt++; // bot right
    }
    else if(row == SIZE-1){ // bot edge
        if(lights[col-1][row-1] == '#') cnt++; // top left
        if(lights[col]  [row-1] == '#') cnt++; // top center
        if(lights[col+1][row-1] == '#') cnt++; // top right

        if(lights[col-1][row] == '#')   cnt++; // center left
        if(lights[col+1][row] == '#')   cnt++; // center right
    }

    //printf("%d/%d: %d\n",col,row,cnt);

    return cnt;
}

int getNeighborsShort(int col, int row){
    int cnt = 0;

    if(col-1 >= 0 && row-1 >= 0)
        if(lights[col-1][row-1] == '#') cnt++; // top left
    if(row-1 >= 0)
        if(lights[col]  [row-1] == '#') cnt++; // top center
    if(col+1 < SIZE && row-1 >= 0)
        if(lights[col+1][row-1] == '#') cnt++; // top right

    if(col-1 >= 0)
        if(lights[col-1][row] == '#')   cnt++; // center left
    if(col+1 < SIZE)
        if(lights[col+1][row] == '#')   cnt++; // center right

    if(col-1 >= 0 && row+1 < SIZE)
        if(lights[col-1][row+1] == '#') cnt++; // bot left
    if(row+1 < SIZE)
        if(lights[col]  [row+1] == '#') cnt++; // bot center
    if(col+1 < SIZE && row+1 < SIZE)
        if(lights[col+1][row+1] == '#') cnt++; // bot right

    return cnt;
}

int countLightsOn(void){
    int cnt = 0;

    for(int row = 0; row < SIZE; row++){
        for(int col = 0; col < SIZE; col++){
            if (lights[col][row] == '#'){
                cnt++;
            }
        }
    }

    return cnt;
}

int day18(int part){
    readFile();

    //printLights();
    //printf("\n");

    if(part != 0){ // four corners always on
        lights[0][0] = '#';
        lights[0][SIZE-1] = '#';
        lights[SIZE-1][0] = '#';
        lights[SIZE-1][SIZE-1] = '#';
    }

    for(int ii = 0; ii < STEPS; ii++){
        step(part);
        //printLights();
        //printf("\n");
    }

    int cnt = countLightsOn();

    printf("Lights on after %d steps: %d\n",STEPS,cnt);

    return cnt;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_ING (4)

#define FALSE   (0)
#define TRUE    (1)

typedef char bool;

struct ingredients{
    int capacity;
    int durability;
    int flavor;
    int texture;
    int calories;
    int amount;
};

struct ingredients ingred[NUM_ING];

int score = 0;

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day15(0);
        }
        else{
            return day15(1);
        }
    }
    else{
        return day15(0);
    }
}

void calcScore(){
    int ii;
    int newScore;
    int cap = 0;
    int dur = 0;
    int flav = 0;
    int text = 0;
    int cal = 0;

    //printf("%03d/%03d/%03d/%03d = %d\n",ingred[0].amount,ingred[1].amount,ingred[2].amount,ingred[3].amount,ingred[0].amount+ingred[1].amount+ingred[2].amount+ingred[3].amount);

    for(ii=0;ii<NUM_ING;ii++){
        cap += ingred[ii].capacity * ingred[ii].amount;
        dur += ingred[ii].durability * ingred[ii].amount;
        flav += ingred[ii].flavor * ingred[ii].amount;
        text += ingred[ii].texture * ingred[ii].amount;
        cal += ingred[ii].calories * ingred[ii].amount;
    }
    if(cap < 0) cap = 0;
    if(dur < 0) dur = 0;
    if(flav < 0) flav = 0;
    if(text < 0) text = 0;
    if(cal < 0) cal = 0;

    newScore = cap*dur*flav*text;

    if(newScore > score && cal == 500)
        score = newScore;
}

void findAmounts(int id, int totalAmount){
    int ii;

    if(id >= NUM_ING-1){
        ingred[id].amount = 100-totalAmount;
        calcScore();
        return;
    }

    for(ii=0;ii<100-totalAmount;ii++){
        ingred[id].amount = ii;
        findAmounts(id+1, totalAmount+ingred[id].amount);
    }
}

void initIngred(){
    ingred[0].capacity = 2;
    ingred[0].durability = 0;
    ingred[0].flavor = -2;
    ingred[0].texture = 0;
    ingred[0].calories = 3;
    ingred[1].capacity = 0;
    ingred[1].durability = 5;
    ingred[1].flavor = -3;
    ingred[1].texture = 0;
    ingred[1].calories = 3;
    ingred[2].capacity = 0;
    ingred[2].durability = 0;
    ingred[2].flavor = 5;
    ingred[2].texture = -1;
    ingred[2].calories = 8;
    ingred[3].capacity = 0;
    ingred[3].durability = -1;
    ingred[3].flavor = 0;
    ingred[3].texture = 5;
    ingred[3].calories = 8;
}

int day15(int part){
    initIngred();
    findAmounts(0, 0);
    printf("%d\n",score);
}

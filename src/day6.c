#include <stdio.h>
#include <stdlib.h>

char lights[1000][1000];

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day6(0);
        }
        else{
            return day6(1);
        }
    }
    else{
        return day6(0);
    }
}

void initLights(){
    int ii,jj;

    for(ii=0;ii<1000;ii++){
        for(jj=1;jj<1000;jj++){
            lights[ii][jj] = 0;
        }
    }
}

void switchLightsA(int x1, int y1, int x2, int y2, char state){
    int ii, jj;

    for(ii=x1;ii<=x2;ii++){
        for(jj=y1;jj<=y2;jj++){
            if(state == 'o'){
                lights[ii][jj] = 1;
            }
            else if(state == 'f'){
                lights[ii][jj] = 0;
            }
            else if(lights[ii][jj] == 1){
                lights[ii][jj] = 0;
            }
            else{
                lights[ii][jj] = 1;
            }
        }
    }
}

void switchLights(int x1, int y1, int x2, int y2, char state){
    int ii, jj;

    for(ii=x1;ii<=x2;ii++){
        for(jj=y1;jj<=y2;jj++){
            if(state == 'o'){
                lights[ii][jj] += 1;
            }
            else if(state == 'f'){
                if(lights[ii][jj] > 0) lights[ii][jj] -= 1;
            }
            else{
                lights[ii][jj] += 2;
            }
        }
    }
}

int countLights(){
    int ii,jj;
    int count = 0;

    for(ii=0;ii<1000;ii++){
        for(jj=1;jj<1000;jj++){
            count += lights[ii][jj];
        }
    }

    return count;
}

int day6(int part)
{
    FILE * input;
    int ii = 0;
    char string[50];
    char cc;
    int x1,x2,y1,y2;

    initLights();

    input = fopen("input.txt", "r");

    while (fgets (string, sizeof(string), input)) {
        printf("line: %s", string);

        if(string[1] == 'u'){
            if(string[6] == 'n'){ // turn on
                sscanf(string,"turn on %d,%d through %d,%d\n",&x1,&y1,&x2,&y2);
                switchLights(x1,y1,x2,y2,'o');
            }
            else if(string[6] == 'f'){ // turn off
                sscanf(string,"turn off %d,%d through %d,%d\n",&x1,&y1,&x2,&y2);
                switchLights(x1,y1,x2,y2,'f');
            }
        }
        else if(string[1] == 'o'){  // toggle
            sscanf(string,"toggle %d,%d through %d,%d\n",&x1,&y1,&x2,&y2);
            switchLights(x1,y1,x2,y2,'t');
        }

        printf("%d/%d/%d/%d\n",x1,y1,x2,y2);
    }

    printf("Lights: %d\n",countLights());

    return 0;
}

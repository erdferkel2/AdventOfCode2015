#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day2(0);
        }
        else{
            return day2(1);
        }
    }
    else{
        return day2(0);
    }
}

int day2(int part)
{
    FILE * input;

    int l,w,h;
    int totalArea;
    int Area;
    int AreaF;
    int ret;
    int Ribbon;
    int totalRibbon;

    ret = 1;
    totalArea = 0;
    totalRibbon = 0;
    input = fopen("input.txt", "r");

    while(ret != 0){
        fscanf(input,"%dx%dx%d",&l,&w,&h);

        if(getc(input) == EOF) break;

        printf("%dx%dx%d\n",l,w,h);

        Area = (2*l*w + 2*w*h + 2*h*l);

        printf("Area: %d\n",Area);

        if(l<=w && l<=h){
            if(w<=h){
                AreaF = l*w;
                Ribbon = 2*l+2*w;
            }
            else{
                AreaF = l*h;
                Ribbon = 2*l+2*h;
            }
        }
        else if(w<=l && w<=h){
            if(l<=h){
                AreaF = w*l;
                Ribbon = 2*w+2*l;
            }
            else{
                AreaF = w*h;
                Ribbon = 2*w+2*h;
            }
        }
        else if(h<=l && h<=w){
            if(l<=w){
                AreaF = h*l;
                Ribbon = 2*h+2*l;
            }
            else{
                AreaF = h*w;
                Ribbon = 2*h+2*w;
            }
        }
        else{
            printf("scured :<\n");
            return -1;
        }

        Area += AreaF;

        Ribbon += l*w*h;

        printf("Ribbon: %d\n",Ribbon);

        printf("AreaExtra: %d\n",AreaF);

        totalArea += Area;
        totalRibbon += Ribbon;
    }



    printf("Area: %d\n",totalArea);
    printf("Ribbon: %d\n",totalRibbon);

    return 0;
}

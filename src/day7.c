#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define NUM_WIRES 339

struct wire{
    char operation;
    int shift;
    char inputA[3];
    char inputB[3];
    char output[3];
    uint16_t result;
    int resultvalid;
};

struct wire wires[NUM_WIRES];

int day7(int part);
void readFile(void);
void initArrays(void);
void printArray(void);
void sortArray(void);
int findWire(char* w);
void resetArray(void);
int computeSignal(char* w);

int main(int argc, const char* argv[]){
    if(argc > 1) {
        int part = 0;
        if (sscanf (argv[1], "%i", &part)!=1){
            printf ("ERROR: argument 1 needs to be an Integer!\n");
        }
        return day7(part);
    }
    else{
        return day7(0);
    }
}

void initArrays(void){
    int ii;
    for (ii=0;ii<NUM_WIRES;ii++){
        wires[ii].operation = 0;
        wires[ii].shift = 0;
        wires[ii].inputA[0] = 0;
        wires[ii].inputA[1] = 0;
        wires[ii].inputA[2] = 0;
        wires[ii].inputB[0] = 0;
        wires[ii].inputB[1] = 0;
        wires[ii].inputB[2] = 0;
        wires[ii].output[0] = 0;
        wires[ii].output[1] = 0;
        wires[ii].output[2] = 0;
        wires[ii].resultvalid = 0;
    }
}

/*
da LSHIFT 1 -> du       op = l
jp RSHIFT 3 -> jr       op = r
eg AND ei -> ej         op = a
au OR av -> aw          op = o
NOT bw -> bx            op = n
0 -> c
lx -> a
*/

// needs newline at end of file
void readFile(void){
    FILE* input;
    input = fopen("input/day7.txt","r");

    char string[20];
    char * delim = " ";

    char* inA;
    char* inB;
    char* out;
    char* op;

    int count = 0;

    while(fgets(string,sizeof(string),input)){
        if(string[0] == 'N'){
            // NOT Operation has only 4 parts
            strtok(string, delim);  // NOT
            inA = strtok(NULL, delim);    // wire in
            strtok(NULL, delim);    // ->
            out = strtok(NULL, delim);    // wire out

            wires[count].operation = 'n';
            wires[count].inputA[0] = inA[0];
            if(inA[1] != 0 && inA[1] != 10) wires[count].inputA[1] = inA[1];
            wires[count].output[0] = out[0];
            if(out[1] != 0 && out[1] != 10) wires[count].output[1] = out[1];

        }
        else{
            inA = strtok(string, delim);  // wire in 1
            op = strtok(NULL, delim);    // Operator
            inB = strtok(NULL, delim);    // wire in 2 or shift amount
            strtok(NULL, delim);    // ->
            out = strtok(NULL, delim);    // wire out

            // find operator:
            if(op[0] == 'L'){ // LSHIFT
                wires[count].operation = 'l';
                wires[count].inputA[0] = inA[0];
                if(inA[1] != 0 && inA[1] != 10) wires[count].inputA[1] = inA[1];
                wires[count].shift = atoi(inB);
                wires[count].output[0] = out[0];
                if(out[1] != 0 && out[1] != 10) wires[count].output[1] = out[1];
            }
            else if(op[0] == 'R'){ // RSHIFT
                wires[count].operation = 'r';
                wires[count].inputA[0] = inA[0];
                if(inA[1] != 0 && inA[1] != 10) wires[count].inputA[1] = inA[1];
                wires[count].shift = atoi(inB);
                wires[count].output[0] = out[0];
                if(out[1] != 0 && out[1] != 10) wires[count].output[1] = out[1];
            }
            else if(op[0] == 'A'){ // AND
                wires[count].operation = 'a';
                wires[count].inputA[0] = inA[0];
                if(inA[1] != 0 && inA[1] != 10) wires[count].inputA[1] = inA[1];
                wires[count].inputB[0] = inB[0];
                if(inB[1] != 0 && inB[1] != 10) wires[count].inputB[1] = inB[1];
                wires[count].output[0] = out[0];
                if(out[1] != 0 && out[1] != 10) wires[count].output[1] = out[1];
            }
            else if(op[0] == 'O'){ // OR
                wires[count].operation = 'o';
                wires[count].inputA[0] = inA[0];
                if(inA[1] != 0 && inA[1] != 10) wires[count].inputA[1] = inA[1];
                wires[count].inputB[0] = inB[0];
                if(inB[1] != 0 && inB[1] != 10) wires[count].inputB[1] = inB[1];
                wires[count].output[0] = out[0];
                if(out[1] != 0 && out[1] != 10) wires[count].output[1] = out[1];
            }
            else if(op[0] == '-'){ // ->, [number] -> [wire] happened
                wires[count].operation = 'x';
                wires[count].inputA[0] = inA[0];
                int c = 1;
                while(inA[c] != 0 && inA[c] != 10) { // line 90: 14146 -> b
                    wires[count].inputA[c] = inA[c];
                    c++;
                }
                wires[count].output[0] = inB[0];
                if(inB[1] != 0 && inB[1] != 10) wires[count].output[1] = inB[1];
            }
        }
        count++;
    }
}

void printArray(void){
    int ii;
    for(ii = 0; ii < NUM_WIRES; ii++){
        switch(wires[ii].operation){
            case 'n':
                printf("NOT %s -> %s\n", wires[ii].inputA, wires[ii].output);
                break;
            case 'l':
                printf("%s LSHIFT %d -> %s\n", wires[ii].inputA, wires[ii].shift, wires[ii].output);
                break;
            case 'r':
                printf("%s RSHIFT %d -> %s\n", wires[ii].inputA, wires[ii].shift, wires[ii].output);
                break;
            case 'a':
                printf("%s AND %s -> %s\n", wires[ii].inputA, wires[ii].inputB, wires[ii].output);
                break;
            case 'o':
                printf("%s OR %s -> %s\n", wires[ii].inputA, wires[ii].inputB, wires[ii].output);
                break;
            case 'x':
                printf("%s -> %s\n", wires[ii].inputA, wires[ii].output);
                break;
        }
    }
}

void sortArray(void){
    // what's that?
}

int findWire(char* w){
    int length;
    int ii;

    if(w[1] == 0) length = 1;
    else length = 2;
    for(ii = 0; ii < NUM_WIRES; ii++){
        if(length == 1){
            if(wires[ii].output[0] == w[0] && wires[ii].output[1] == 0){
                return ii;
            }
        }
        else{
            if(wires[ii].output[0] == w[0] && wires[ii].output[1] == w[1]){
                return ii;
            }
        }
    }

    return -1;
}

int computeSignal(char* w){
    if(w[0] == '0' || w[0] == '1' || w[0] == '2' || w[0] == '3' || w[0] == '4' || w[0] == '5' || w[0] == '6' || w[0] == '7' || w[0] == '8' || w[0] == '9'){
        return atoi(w);
    }

    int index = findWire(w);

    if(wires[index].resultvalid == 1){
        return wires[index].result;
    }

    switch(wires[index].operation){
        case 'n':
            wires[index].result = ~computeSignal(wires[index].inputA);
            wires[index].resultvalid = 1;
            //printf("%s: NOT %s =  %d\n",w,wires[index].inputA,wires[index].result);
            //printf("%s: %d\n",w,wires[index].result);
            return wires[index].result;
            break;
        case 'l':
            wires[index].result = computeSignal(wires[index].inputA) << wires[index].shift;
            wires[index].resultvalid = 1;
            //printf("%s: %s LSHIFT %d =  %d\n",w,wires[index].inputA,wires[index].shift,wires[index].result);
            //printf("%s: %d\n",w,wires[index].result);
            return wires[index].result;
            break;
        case 'r':
            wires[index].result = computeSignal(wires[index].inputA) >> wires[index].shift;
            wires[index].resultvalid = 1;
            //printf("%s: %s RSHIFT %d =  %d\n",w,wires[index].inputA,wires[index].shift,wires[index].result);
            //printf("%s: %d\n",w,wires[index].result);
            return wires[index].result;
            break;
        case 'a':
            wires[index].result = computeSignal(wires[index].inputA) & computeSignal(wires[index].inputB);
            wires[index].resultvalid = 1;
            //printf("%s: %s AND %s =  %d\n",w,wires[index].inputA,wires[index].inputB,wires[index].result);
            //printf("%s: %d\n",w,wires[index].result);
            return wires[index].result;
            break;
        case 'o':
            wires[index].result = computeSignal(wires[index].inputA) | computeSignal(wires[index].inputB);
            wires[index].resultvalid = 1;
            //printf("%s: %s OR %s =  %d\n",w,wires[index].inputA,wires[index].inputB,wires[index].result);
            //printf("%s: %d\n",w,wires[index].result);
            return wires[index].result;
            break;
        case 'x':
            wires[index].result = computeSignal(wires[index].inputA);
            wires[index].resultvalid = 1;
            //printf("%s: assign %s =  %d\n",w,wires[index].inputA,wires[index].result);
            //printf("%s: %d\n",w,wires[index].result);
            return wires[index].result;
            break;
    }

    return -1;
}

void resetArray(void){
    int ii;
    for(ii = 0; ii < NUM_WIRES; ii++){
        wires[ii].result = 0;
        wires[ii].resultvalid = 0;
    }
}

int day7(int part){

    initArrays();
    readFile();

    uint16_t result = computeSignal("a");

    if(part == 0){
        printf("Signal on wire A: %d\n",result);
        return result;
    }

    resetArray();

    wires[findWire("b")].result = result;
    wires[findWire("b")].resultvalid = 1;

    result = computeSignal("a");

    printf("Signal on wire A: %d\n",result);
    return result;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day12(0);
        }
        else{
            return day12(1);
        }
    }
    else{
        return day12(0);
    }
}

int processObj(FILE* fp){
    char cc = 1;
    int sum = 0;
    char n[10];
    char number = FALSE;
    int pos = 0;
    int strState = 0;
    char red = 0;
    char string = 0;
    char str[10];
    char inArray = 0;

    while(cc != '}'){
        cc = getc(fp);
        if(cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7' || cc == '8' || cc == '9' || cc == '-'){
            // start of number
            number = TRUE;
            n[pos] = cc;
            pos++;
        }
        else{
            if(number == TRUE){
                n[pos] = 0;
                sum += atoi(n);
                //printf("sum: %d\n",sum);
            }
            number = FALSE;
            pos = 0;
        }

        if(cc == '{'){
            sum += processObj(fp);
        }

        if(string == 1){
            sprintf(str,"%s%c",str,cc);
        }

        if(cc == '['){
            inArray++;
        }
        if(cc == ']'){
            inArray--;
        }

        if(cc == '"'){
            if(string == 1){
                string = 0;
                if(str[0] == '"' && str[1] == 'r' && str[2] == 'e' && str[3] == 'd' && str[4] == '"'){
                    if(inArray == 0){
                        red = 1;
                        printf("red 1\n");
                    }
                }
                //printf("%s\n",str);
                sprintf(str,"");
            }
            else{
                string = 1;
                sprintf(str,"\"");
            }
        }

    }

    if(red == 0){
            printf("returning %d\n",sum);
        return sum;
    }
    else if(red == 1){
        printf("returning %d\n",0);
        return 0;
    }
}

void readFile(){
    FILE* input;
    input = fopen("input.txt","r");

    char cc = 1;
    char number;
    char n[10];
    int pos = 0;
    int sum = 0;

    number = FALSE;

    while(cc != EOF){
        cc = getc(input);
        if(cc == '0' || cc == '1' || cc == '2' || cc == '3' || cc == '4' || cc == '5' || cc == '6' || cc == '7' || cc == '8' || cc == '9' || cc == '-'){
            // start of number
            number = TRUE;
            n[pos] = cc;
            pos++;
        }
        else{
            if(number == TRUE){
                n[pos] = 0;
                sum += atoi(n);
            }
            number = FALSE;
            pos = 0;
        }

        //==============================================

        if(cc == '{'){
            sum += processObj(input);
        }

    }

    printf("sum: %d\n",sum);
}

int day12 (int part){
    readFile();

    return 0;
}

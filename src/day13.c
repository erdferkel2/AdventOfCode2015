#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define NODE_COUNT  (9)
#define NAME_LENGTH (20)

int adjacencyMatrix[NODE_COUNT][NODE_COUNT];

// string array storing names of nodes
char *node[NODE_COUNT][NAME_LENGTH];

struct path{
    int length;
    int nodes[NODE_COUNT];
};

int bestDist = 0;

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day13(0);
        }
        else{
            return day13(1);
        }
    }
    else{
        return day13(0);
    }
}

void printAdjMatrix(){
    int ii, jj;

    for(ii=0;ii<NODE_COUNT;ii++){
        printf("     %d",ii);
    }
    printf("\n");

    for(ii=0;ii<NODE_COUNT;ii++){
        printf("%d |",ii);
        for(jj=0;jj<NODE_COUNT;jj++){
            printf(" %03d |", adjacencyMatrix[ii][jj]);
        }
        printf("\n");
    }
}

int getNodeIndex(char* name){
    static nodesInMemory = 0;
    int ii;

    for(ii=0;ii<nodesInMemory;ii++){
        if(strcmp(node[ii],name) == 0){
            return ii;
        }
    }
    strcpy(node[nodesInMemory] , name);

    nodesInMemory++;

    return nodesInMemory-1;
}

void getNodeName(int index, char* name){
    name = node[index];
}

void initArrays(){
    int ii, jj;

    for(ii=0;ii<NODE_COUNT;ii++){
        for(jj=0;jj<NODE_COUNT;jj++){
            adjacencyMatrix[ii][jj] = 0;
        }
    }

    for(ii=0;ii<NODE_COUNT;ii++){
        for(jj=0;jj<NAME_LENGTH;jj++){
            node[ii][jj] = 0;
        }
    }
}

// needs newline at end of file
void readFile(){
FILE * input;
    char string[100];
    char* nodeA;
    char* nodeB;
    char* weight;
    char* dir;

    int nodeStart, nodeStop;
    int edgeWeight;

    char* delim = " ";

    input = fopen("input.txt", "r");

    while(fgets(string,sizeof(string),input)){
        // get rid of \n and . at the end of a line
        string[strlen(string)-2] = 0;
        // scan string
        nodeA = strtok(string, delim);
        strtok(NULL, delim); // skip " would "
        dir = strtok(NULL, delim); // loose or gain
        weight = strtok(NULL, delim);
        strtok(NULL, delim); // skip " happiness "
        strtok(NULL, delim); // skip " units "
        strtok(NULL, delim); // skip " by "
        strtok(NULL, delim); // skip " sitting "
        strtok(NULL, delim); // skip " next "
        strtok(NULL, delim); // skip " to "
        nodeB = strtok(NULL, delim);

        nodeStart = getNodeIndex(nodeA);
        nodeStop = getNodeIndex(nodeB);
        edgeWeight = atoi(weight);

        if(dir[0] == 'l') edgeWeight = -edgeWeight;

        adjacencyMatrix[nodeStart][nodeStop] = edgeWeight;
        //printf("%d/%d  -  %d\n",nodeStart,nodeStop,edgeWeight);
    }
}

int pathContains(struct path *p, int node){
    int ii;
    for(ii=0;ii<p->length;ii++){
        if(p->nodes[ii] == node)
            return 1;
    }
    return 0;
}

int addToPath(struct path *p, int node){
    p->nodes[p->length] = node;
    p->length++;
}

void calculateDistance(struct path *p){
    int dist = 0;

    int ii;
    for(ii=1;ii<p->length;ii++){
        dist += adjacencyMatrix[p->nodes[ii-1]][p->nodes[ii]];
        dist += adjacencyMatrix[p->nodes[ii]][p->nodes[ii-1]];

    }

    dist += adjacencyMatrix[p->nodes[0]][p->nodes[p->length-1]];
    dist += adjacencyMatrix[p->nodes[p->length-1]][p->nodes[0]];

    if(dist > bestDist){
        bestDist = dist;

    }
}

void printPath(struct path *p){
    int ii;
    for(ii=0;ii<p->length;ii++){
        printf("%d-",p->nodes[ii]);
    }
    printf("\n");
}

void createPath(int depth, struct path p){
    int ii;

    if(depth >= NODE_COUNT){
        calculateDistance(&p);
        return;
    }

    for (ii=0;ii<NODE_COUNT;ii++) {
        printf("",ii); // segfault if you remove this
        struct path np = p;
        if (!pathContains(&np, ii)){
            addToPath(&np, ii);
            createPath(depth+1, np);
        }
    }
}


int day13(int part){
    struct path p;

    initArrays();
    readFile();
    // hacky fix
    adjacencyMatrix[0][0] = 0;
    adjacencyMatrix[0][1] = -57;
    printAdjMatrix();

    p.length = 0;
    createPath(0,p);

    printf("best: %d\n",bestDist);

    return 0;
}

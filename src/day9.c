#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define NODE_COUNT  (8)
#define NAME_LENGTH (20)

#define SHOWOFF 1

int adjacencyMatrix[NODE_COUNT][NODE_COUNT];

// string array storing names of nodes
char *node[NODE_COUNT][NAME_LENGTH];

struct path{
    int length;
    int nodes[NODE_COUNT];
};

#ifdef SHOWOFF
int bestDist = 0;
#else
int bestDist = INT_MAX;
#endif

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day9(0);
        }
        else{
            return day9(1);
        }
    }
    else{
        return day9(0);
    }
}

void printAdjMatrix(){
    int ii, jj;

    for(ii=0;ii<NODE_COUNT;ii++){
        printf("     %d",ii);
    }
    printf("\n");

    for(ii=0;ii<NODE_COUNT;ii++){
        printf("%d |",ii);
        for(jj=0;jj<NODE_COUNT;jj++){
            printf(" %03d |", adjacencyMatrix[ii][jj]);
        }
        printf("\n");
    }
}

int getNodeIndex(char* name){
    static nodesInMemory = 0;
    int ii;

    for(ii=0;ii<nodesInMemory;ii++){
        if(strcmp(node[ii],name) == 0){
            return ii;
        }
    }
    strcpy(node[nodesInMemory] , name);

    nodesInMemory++;

    return nodesInMemory-1;
}

void getNodeName(int index, char* name){
    name = node[index];
}

void initArrays(){
    int ii, jj;

    for(ii=0;ii<NODE_COUNT;ii++){
        for(jj=0;jj<NODE_COUNT;jj++){
            adjacencyMatrix[ii][jj] = 0;
        }
    }

    for(ii=0;ii<NODE_COUNT;ii++){
        for(jj=0;jj<NAME_LENGTH;jj++){
            node[ii][jj] = 0;
        }
    }
}

// ==READ THIS==
// input requires a newline
// (cause I'm a bad programmer)
void readFile(){
FILE * input;
    char string[56];
    char* nodeA;
    char* nodeB;
    char* weight;

    int nodeStart, nodeStop;
    int edgeWeight;

    char* delim = " ";

    input = fopen("input.txt", "r");

    while(fgets(string,sizeof(string),input)){
        // get rid of \n
        string[strlen(string)-1] = 0;
        // scan string
        nodeA = strtok(string, delim);
        strtok(NULL, delim); // skip " to "
        nodeB = strtok(NULL, delim);
        strtok(NULL, delim); // skip " = "
        weight = strtok(NULL, delim);

        nodeStart = getNodeIndex(nodeA);
        nodeStop = getNodeIndex(nodeB);
        edgeWeight = atoi(weight);

        adjacencyMatrix[nodeStart][nodeStop] = edgeWeight;
        adjacencyMatrix[nodeStop][nodeStart] = edgeWeight;
    }
}

int pathContains(struct path *p, int node){
    int ii;
    for(ii=0;ii<p->length;ii++){
        if(p->nodes[ii] == node)
            return 1;
    }
    return 0;
}

int addToPath(struct path *p, int node){
    p->nodes[p->length] = node;
    p->length++;
}

void calculateDistance(struct path *p){
    int dist = 0;

    int ii;
    for(ii=1;ii<p->length;ii++){
        dist += adjacencyMatrix[p->nodes[ii-1]][p->nodes[ii]];

    }
#ifdef SHOWOFF
    if(dist > bestDist)
#else
    if(dist < bestDist)
#endif
        bestDist = dist;
}

void printPath(struct path *p){
    int ii;
    for(ii=0;ii<p->length;ii++){
        printf("%d-",p->nodes[ii]);
    }
    printf("\n");
}

void createPath(int depth, struct path p){
    int ii;

    if(depth >= NODE_COUNT){
        calculateDistance(&p);
        return;
    }

    for (ii=0;ii<NODE_COUNT;ii++) {
        struct path np = p;
        if (!pathContains(&np, ii)){
            addToPath(&np, ii);
            createPath(depth+1, np);
        }
    }
}


int day9(int part){
    struct path p;

    initArrays();
    readFile();
    printAdjMatrix();

    p.length = 0;
    //addToPath(&p,0);
    createPath(0,p);

    printf("%d\n",bestDist);

    return 0;
}

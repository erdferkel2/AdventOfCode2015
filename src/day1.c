#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day1(0);
        }
        else{
            return day1(1);
        }
    }
    else{
        return day1(0);
    }
}

int day1(int part)
{
    FILE * input;
    int floor, pos;
    char cc;

    floor = 0;
    pos = 1;
    input = fopen("input/day1.txt", "r");


    while(cc != EOF){
        cc = getc(input);
        if(cc == '(')
            floor++;
        else if(cc == ')')
            floor--;

            if(floor < 0 && part == 1){
                printf("DAY 1: entered basement @ %d\n",pos);
                return pos;
            }

            pos++;
    }

    printf("DAY 1: floor: %d\n",floor);

    return floor;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LOOP_COUNT 50

int main(int argc, const char* argv[]){
    if(argc > 1) {
        if(argv[1] == 0){
            return day10(0);
        }
        else{
            return day10(1);
        }
    }
    else{
        return day10(0);
    }
}

int day10(int part){
    static char work[6000000];
    static char next[6000000];

    char input[] = "1113222113";

    char last;
    int count;

    int ii, jj;
    int writePos = 0;

    int len;

    char msg[1000];

    // save input
    strcpy(work, input);

    len = strlen(work);

    for(ii=0;ii<LOOP_COUNT;ii++){
        char last = work[0];
        count = 1;
        strcpy(next, "");

        writePos = 0;

        for(jj=1;jj<len;jj++){
            if(work[jj] == last){
                count++;
            }
            else{
                sprintf(msg,"%d%c",count,last);

                next[writePos] = msg[0];
                next[writePos+1] = msg[1];

                writePos += 2;

                last = work[jj];
                count = 1;
            }
        }

        // safeguard, should never be entered
        if(count > 9){
            printf("too many chars are the same!");
            while(1);
        }

        sprintf(msg,"%d%c",count,last);

        next[writePos] = msg[0];
        next[writePos+1] = msg[1];

        writePos += 2;

        len = writePos;

        strcpy(work, next);
        printf("%d / %d\n",ii,strlen(work));
    }

    return 0;
}
